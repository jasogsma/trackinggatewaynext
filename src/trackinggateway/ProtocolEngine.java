/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.logging.Level;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import trackinggateway.Entry;

/**
 *
 * @author stephen
 */
public class ProtocolEngine {

    private final static Logger LOGGER = Logger.getLogger("TIG");
    static final String HEXES = "0123456789ABCDEF";
    private Registrations registrations = new Registrations();
    SecretKeySpec skeySpec;
    Cipher cipher;
    IvParameterSpec ivParameterSpec;
    byte[] keyBytes = new byte[16];
    private String m_reportURL;
    
    public ProtocolEngine(String key, String reportURL) {
        LOGGER.info("ProtocolEngine()");
        m_reportURL = reportURL;
        try {
            byte[] byteStr = key.getBytes("UTF-8");
            System.arraycopy(byteStr, 0, keyBytes, 0, byteStr.length);
            skeySpec = new SecretKeySpec(keyBytes, "AES");
            ivParameterSpec = new IvParameterSpec(keyBytes);
            cipher = Cipher.getInstance("AES/ECB/NoPadding");

            //Look at http://stackoverflow.com/questions/28025742/encrypt-and-decrypt-a-string-with-aes-128
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    public static String byteArrayToHexString(byte[] raw) {
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F))).append(" ");
        }
        return hex.toString();
    }

    public PDU handleBytes(byte[] data, int cnt) {
        /*
         byte[] decrypted = new byte[cnt];
         byte[] previous = new byte[16];
         byte[] plaintext = new byte[16];
         int blockcount = 0;

         System.arraycopy(keyBytes, 0, previous, 0, 16);
         try {
         cipher.init(Cipher.DECRYPT_MODE, skeySpec);
         } catch (InvalidKeyException ex) {
         Logger.getLogger(ProtocolEngine.class.getName()).log(Level.SEVERE, null, ex);
         }

         while (cnt > 0) {
         try {
         plaintext = cipher.doFinal(data, blockcount * 16, 16);
         } catch (IllegalBlockSizeException ex) {
         LOGGER.log(SEVERE, ex.toString());
         } catch (BadPaddingException ex) {
         LOGGER.log(SEVERE, ex.toString());
         }            
         for (int i = 0; i < 16; i++) {
         plaintext[i] ^= previous[i];
         }
         System.arraycopy(data, blockcount * 16, previous, 0, 16);
         System.arraycopy(plaintext, 0, decrypted, blockcount * 16, 16);
         blockcount++;
         cnt -= 16;
         }
         */
        byte[] decrypted = decrypt(data, cnt);
        return new PDU(ByteBuffer.wrap(decrypted));

    }

    public byte[] decrypt(byte[] data, int cnt) {
        byte[] decrypted = new byte[cnt];
        byte[] previous = new byte[16];
        byte[] plaintext = new byte[16];
        int blockcount = 0;

        System.arraycopy(keyBytes, 0, previous, 0, 16);
        try {
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        } catch (InvalidKeyException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        while (cnt > 0) {
            try {
                plaintext = cipher.doFinal(data, blockcount * 16, 16);

                for (int i = 0; i < 16; i++) {
                    plaintext[i] ^= previous[i];
                }
                System.arraycopy(data, blockcount * 16, previous, 0, 16);
                System.arraycopy(plaintext, 0, decrypted, blockcount * 16, 16);
                blockcount++;
                cnt -= 16;
            } catch (IllegalBlockSizeException ex) {
                LOGGER.log(SEVERE, ex.toString());
                return null;
            } catch (BadPaddingException ex) {
                LOGGER.log(SEVERE, ex.toString());
                return null;
            }
        }
        return decrypted;
    }

    public byte[] encrypt(byte[] data, int cnt) {
        byte[] encrypted = new byte[cnt + 16];
        byte[] previous = new byte[16];
        byte[] plaintext = new byte[cnt + 16];
        byte[] ciphertext = null;
        int bytesout = 0, blockcount = 0;

        System.arraycopy(keyBytes, 0, previous, 0, 16);

        try {
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        } catch (InvalidKeyException ex) {
            LOGGER.log(Level.SEVERE, ex.toString());
        }

        while (cnt > 0) {
            if (cnt >= 16) {
                System.arraycopy(data, blockcount * 16, plaintext, 0, 16);
                bytesout += 16;
                cnt -= 16;
            } else {
                System.arraycopy(data, blockcount * 16, plaintext, 0, cnt);
                for (int i = cnt; i < 16; i++) {
                    plaintext[i] = (byte) (Math.random() * 250 + 1);
                }
                bytesout += 16;
                cnt = 0;
            }

            // Xor with the previous ciphertext block                                                                                                                                          
            for (int i = 0; i < 16; i++) {
                plaintext[i] ^= previous[i];
            }

            try {
                // Perform the AES encryption
                ciphertext = cipher.doFinal(plaintext, 0, 16);
            } catch (IllegalBlockSizeException ex) {
                LOGGER.log(Level.SEVERE, ex.toString());
            } catch (BadPaddingException ex) {
                LOGGER.log(Level.SEVERE, ex.toString());
            }

            System.arraycopy(ciphertext, 0, previous, 0, 16);
            System.arraycopy(ciphertext, 0, encrypted, blockcount * 16, 16);
            blockcount++;
        }
        byte[] toReturn = new byte[bytesout];
        System.arraycopy(encrypted, 0, toReturn, 0, bytesout);
        return toReturn;
    }

    
    public PDU receivePDU(PDU pdu, String ipaddress) {
        PDU answerPdu = null;
        int idf = -1;

        if (pdu.isValid()) {

            switch (pdu.messageId()) {
                case PDU.TRACK_REGISTER_REQ:
                    LOGGER.info("TRACK_REGISTER_REQ");
                    idf = (int) pdu.head.getRegisterId();
                    LOGGER.info("RegisterID:" + idf);
                    if (pdu.head.getRegisterId() != 0) {
                        LOGGER.info("RegisterId non-zero, aborting!");
                        return answerPdu;
                    }

                    LOGGER.info("IMSI:" + byteArrayToHexString(pdu.m_imsi));
                    LOGGER.info("terminalId:" + byteArrayToHexString(pdu.m_terminalId));
                    long id = registrations.add(ipaddress, pdu.m_imsi, pdu.m_terminalId, pdu.getCmdValid());
                    LOGGER.info("Registrered with id:" + id);

                    // Generate a TRACK_REGISTER_ACK
                    answerPdu = PDU.RegisterAck(id);
                    break;

                case PDU.TRACK_POS_EXT_IND:
                    LOGGER.info("TRACK_POS_EXT_IND");
                    idf = (int) pdu.head.getRegisterId();
                    LOGGER.info("RegisterID:" + idf);

                    if (idf == 0) {
                        LOGGER.info("RegisterId zero, aborting!");                        
                    }
                    
                    Entry entry = registrations.get(idf);
                    if (idf != 0 && entry != null) {
                        LOGGER.info("Reporting to LocPoint");
                       reportToLocpoint(entry.getIpAddress(), entry.getImsi(),
                               entry.getTerminalId(), pdu.m_timeStamp, 
                               pdu.getLatitude(), pdu.getLongitude(), 
                               pdu.m_altitude, pdu.m_speed, pdu.m_course);
                    } else {
                        LOGGER.info("Forcing not registered TOB to register");
                        answerPdu = PDU.CmdRegisterReq(idf,(byte)0);
                        break;   
                    }
            }
        }
        return answerPdu;
    }

    
    void reportToLocpoint(String ipaddress, byte[] imsi, byte[] terminalId, int timeStamp, double latitude, double longitude, double altitude, float speed, float heading) {
        String postthis;

        String imsiStr = String.format("%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", imsi[0], imsi[1], imsi[2], imsi[3], imsi[4], imsi[5], imsi[6], imsi[7], imsi[8], imsi[9], imsi[10], imsi[11], imsi[12], imsi[13], imsi[14]);
        String imeiStr = String.format("%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", terminalId[0], terminalId[1], terminalId[2], terminalId[3], terminalId[4], terminalId[5], terminalId[6], terminalId[7], terminalId[8], terminalId[9], terminalId[10], terminalId[11], terminalId[12], terminalId[13], terminalId[14]);

        LOGGER.info("reportToLocpoint() - " + imsiStr + "/" + imeiStr + ", timestamp: " + timeStamp);

        if (altitude != 0) {
            postthis = String.format(Locale.US, "id=%s&hw_id=%s&ipaddress=%s&latitude=%2.4f&longitude=%2.4f&altitude=%2.4f&speed=%2.1f&heading=%2.1f&timestamp=%d&type=EXTENDED",
                    imsiStr, imeiStr, ipaddress, latitude, longitude, altitude, speed, heading, timeStamp);
        } else {
            postthis = String.format(Locale.US, "id=%s&hw_id=%s&ipaddress=%s&latitude=%2.4f&longitude=%2.4f&speed=%2.1f&heading=%2.1f&timestamp=%d&type=EXTENDED",
                    imsiStr, imeiStr, ipaddress, latitude, longitude, speed, heading, timeStamp);
        }
        LOGGER.info("Sending to Locpoint:" + postthis);

        String targetURL = m_reportURL; //"http://localhost/bgan";
        try {

            //Create connection
            URL url = new URL(targetURL);
            LOGGER.info("Sending to " + url.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(postthis.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(postthis);
            wr.close();

            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+ 
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            LOGGER.info("Responsecode: " + connection.getResponseCode());          
        } catch (Exception e) {
            LOGGER.info("Exception while posting to LocPoint : " + e);
        } finally {
        }

    }
}
