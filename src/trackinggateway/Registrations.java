/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import trackinggateway.Entry;

/**
 *
 * @author stephen
 */
public class Registrations {
    private final static Logger LOGGER = Logger.getLogger("TIG");
    private int lastRegistration = 0;
                
    private Map<Integer,Entry> registrations = new HashMap<>();

    
    public long add(String ipaddress, byte[]imsi, byte[]terminalId, boolean cmdValid) 
    {
        //if(registrations.size() < lastRegistration) {
       //     registrations.
       // }
        lastRegistration = lastRegistration + 1;
        Entry newRegistration = new Entry(ipaddress, imsi, terminalId, cmdValid, lastRegistration);
        
        // TODO: Check that a matching entry is not aready present
        registrations.put(lastRegistration, newRegistration);
        
        return lastRegistration;
    }
    
    public Entry get(int id)
    {
         return (registrations.get(id));
    }
    
    
}
