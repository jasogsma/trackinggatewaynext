/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * NYESTE!
 * @author stephen.aaskov
 */
public class TrackingGateway extends Thread {
    static final String VERSION = "2.0-4. Sep 24/2018";
    private Logger LOGGER = Logger.getLogger("TIG");
    static final String HEXES = "0123456789ABCDEF";
    private final int m_port;
    private final String m_key;
    private boolean m_running;
    private DatagramSocket serverSocket;
    private String m_reportURL;

    public TrackingGateway(int port, String key, String reportURL) {
        m_port = port;
        m_key = key;
        m_reportURL = reportURL;
    }

    public void stopRunning() {
        m_running = false;
        serverSocket.close();
    }

    public static String byteArrayToHexString(byte[] raw) {
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F))).append(" ");
        }
        return hex.toString();
    }

    @Override
    public void run() {

        FileHandler fh;

        try {
 
            // This block configure the logger with handler and formatter  
            fh = new FileHandler("TIG_" + m_port + ".log", true);
            LOGGER.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            // the following statement is used to log any messages  
            LOGGER.setUseParentHandlers(false);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        m_running = true;
        try {
            server();
        } catch (IOException ex) {
            Logger.getLogger(TrackingGateway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void server() throws SocketException, IOException {
        LOGGER.info("TIG " + VERSION);
        LOGGER.info("Server starting, listening at port " + m_port);
        serverSocket = new DatagramSocket(m_port);
        ProtocolEngine pe = new ProtocolEngine(m_key, m_reportURL);

        while (m_running) {
            byte[] rxData = new byte[1024];
            DatagramPacket rxPacket = new DatagramPacket(rxData, rxData.length);
            try {
                serverSocket.receive(rxPacket);
            } catch (Exception e) {
                LOGGER.info("Exception while reciveing:" + e);
            }
            LOGGER.info("RX from " + rxPacket.getAddress() + " port " + rxPacket.getPort() + " length:" + rxPacket.getLength());
            if (rxPacket.getLength() < 1) {
                return;
            }
            PDU rxPDU = pe.handleBytes(rxPacket.getData(), rxPacket.getLength());
            if (rxPDU != null) {
                System.out.println(rxPacket.getAddress() + ":" + rxPacket.getPort() + "->" + rxPDU);

                PDU txPDU = pe.receivePDU(rxPDU, rxPacket.getAddress().getHostAddress());
                if (txPDU != null) {                    
                    // todo: use bytebuffer.wrap() below....
                    ByteBuffer buf = ByteBuffer.allocate(100);
                    txPDU.encodeToBuffer(buf);
                    int offset = buf.position();
                    buf.rewind();

                    byte[] txData = buf.array();
                    buf.get(txData);
                    byte[] encrypted = pe.encrypt(txData, offset);
                    System.out.println(rxPacket.getAddress() + ":" + rxPacket.getPort() + "<-" + txPDU);
                    LOGGER.info("Sending answer to " + rxPacket.getAddress() + ":" + rxPacket.getPort());
                    DatagramPacket txPacket = new DatagramPacket(encrypted, encrypted.length, rxPacket.getAddress(), rxPacket.getPort());
                    serverSocket.send(txPacket);
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer port = 7474;
        String key = "poelse";
        String host = "localhost";

        System.setProperty("java.util.logging.SimpleFormatter.format","[%1$tF %1$tT] [%4$-7s] %5$s %n");

        System.out.println(args[0]);
        if (args.length>0 && "test".compareToIgnoreCase(args[0]) == 0) {
            System.out.println("Running in test-mode\n");
        }
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].compareTo("-p") == 0) {
                port = Integer.parseInt(args[i + 1]);
                i++;
            }
            if (args[i].compareTo("-h") == 0) {
                host = args[i + 1];
                i++;
            }
            if (args[i].compareTo("-k") == 0) {
                key = args[i + 1];
                i++;
            }
        }
        String URL = "http://" + host + "/bgan";
        System.out.println("TIG starting - " + VERSION);
        System.out.println("Port:" + port + ", URL:" + URL);

        TrackingGateway t = new TrackingGateway(port, key, URL);
        t.run();
    }

}
