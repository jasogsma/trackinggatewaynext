/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author stephen.aaskov
 */
public class MessageHeader {

    private long m_registerId;
    private short m_messageId;
    private boolean m_valid = false;

    
    public MessageHeader(ByteBuffer buf) {
        buf.order(ByteOrder.BIG_ENDIAN);
        decodeFromBuffer(buf);
    }

    public MessageHeader(int registerId, short messageId) {
        m_registerId = registerId;
        m_messageId = messageId;
        m_valid = true;
    }

    public void encodeToBuffer(ByteBuffer buf) {
        buf.put((byte) 0xfe);
        buf.put((byte) 0xde);

        buf.putInt((int)m_registerId);
        buf.putShort(m_messageId);
    }

    public void decodeFromBuffer(ByteBuffer buf) {

        byte b1 = (byte) buf.get();
        byte b2 = (byte) buf.get();

        if (b1 == (byte) 0xfe && b2 == (byte) 0xde) {
            m_valid = true;
            m_registerId = buf.getInt();
            m_messageId = buf.getShort();
        }
    }

    public long getRegisterId()
    {
        return m_registerId;
    }
    
    public short getMessageId()
    {
        return m_messageId;
    }
    
    /**
     * @return the m_valid
     */
    public boolean isValid() {
        return m_valid;
    }

}
