/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

/**
 *
 * @author stephen
 */

public class Entry {
        private byte[] m_imsi;
        private byte[] m_terminalId;
        private int id;        
        private boolean cmd_valid;
        private String m_ipaddress;
        
        public Entry(String ipaddress, byte[]imsi, byte[]terminalId, boolean cmdValid, int registerId)
        {
            m_ipaddress = ipaddress;
            m_imsi = imsi;
            m_terminalId = terminalId;
            cmd_valid = cmdValid;
            id = registerId;
        }                

        /**
         * @return the imsi
         */
        public byte[] getImsi() {
            return m_imsi;
        }

        /**
         * @return the terminalId
         */
        public byte[] getTerminalId() {
            return m_terminalId;
        }
        
        public String getIpAddress() {
            return m_ipaddress;
        }
    }