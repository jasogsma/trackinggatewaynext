/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackinggateway;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Logger;

/**
 *
 * @author stephen.aaskov
 */
public class PDU {

    static final short TRACK_REG = 0x0100;
    static final short TRACK_REGISTER_REQ = 0x0101;
    static final short TRACK_REGISTER_ACK = 0x0102;

    static final short TRACK_POS = 0x0200;
    static final short TRACK_POS_COM_IND = 0x0201;
    static final short TRACK_POS_EXT_IND = 0x0202;
    static final short TRACK_POS_EXT_3D_IND = 0x0203;

    static final short TRACK_CMD = 0x0300;
    static final short TRACK_CMD_REG_REQ = TRACK_CMD | 0x6;

    MessageHeader head;
    byte version = 1;
    byte[] m_imsi = new byte[16];
    byte[] m_terminalId = new byte[16];
    byte m_cmdValid;
    byte m_transId;

    int m_timeStamp;
    int m_latitude;
    int m_longitude;
    short m_speed;
    short m_course;
    short m_altitude;
    short m_userDatLength;

    private final static Logger LOGGER = Logger.getLogger("TIG");

    public float getLongitude() {
        return (float) (m_longitude * 0.00001);
    }

    public float getLatitude() {
        return (float) (m_latitude * 0.00001);
    }

    public void setLongitude(float longitude) {
        m_longitude = (int) (longitude / 0.00001);
    }

    public void setLatitude(float latitude) {
        m_latitude = (int) (latitude / 0.00001);
    }

    public boolean getCmdValid() {
        return m_cmdValid == 1;
    }

    public byte getVersion() {
        return version;
    }

    public String toString() {
        String name = messageName();
        String content = "timestamp:" + m_timeStamp + ",m_latitude:" + this.getLatitude() + ",m_longitude:" + this.getLongitude() + ",m_speed:" + m_speed + ",m_course:" + m_course + ",m_altitude:" + m_altitude + ",m_userDatLength:" + m_userDatLength;
        return name + "\n" + content;
    }

    public short messageId() {
        return head.getMessageId();
    }

    public String messageName() {
        switch (head.getMessageId()) {
            case TRACK_REGISTER_REQ:
                return "TRACK_REGISTER_REQ";
            case TRACK_REGISTER_ACK:
                return "TRACK_REGISTER_ACK";
            case TRACK_POS_COM_IND:
                return "TRACK_POS_COM_IND";
            case TRACK_POS_EXT_IND:
                return "TRACK_POS_EXT_IND";
            case TRACK_POS_EXT_3D_IND:
                return "TRACK_POS_EXT_3D_IND";
            case TRACK_CMD_REG_REQ:
                return "TRACK_CMD_REG_REQ";

        }
        return "N/A";
    }

    boolean isValid() {
        return head.isValid();
    }

    public PDU(MessageHeader header) {
        head = header;
    }

    public PDU(ByteBuffer buf) {
        LOGGER.info("New PDU, calling decodeFromBuffer()");
        decodeFromBuffer(buf);
    }

    public static PDU RegisterReq(long registerId, byte[] imsi, byte[] terminalId, int cmdValid) {
        PDU pdu = new PDU(new MessageHeader((int) registerId, (short) TRACK_REGISTER_REQ));
        pdu.m_imsi = imsi;
        pdu.m_terminalId = terminalId;
        pdu.m_cmdValid = (byte) cmdValid;

        return pdu;
    }

    public static PDU RegisterAck(long registerId) {
        PDU pdu = new PDU(new MessageHeader((int) registerId, (short) TRACK_REGISTER_ACK));

        return pdu;
    }

    public static PDU CmdRegisterReq(long registerId, byte transId) {
        PDU pdu = new PDU(new MessageHeader((int) registerId, (short) TRACK_CMD_REG_REQ));
        pdu.m_transId = transId;

        return pdu;
    }

    public void encodeToBuffer(ByteBuffer buf) {
        buf.order(ByteOrder.BIG_ENDIAN);
        head.encodeToBuffer(buf);

        switch (head.getMessageId()) {
            case TRACK_REGISTER_REQ:
                buf.put(m_imsi, 0, m_imsi.length);
                buf.put(m_terminalId, 0, m_terminalId.length);
                buf.put((byte) m_cmdValid);
                break;

            case TRACK_REGISTER_ACK:
                buf.putInt((int) head.getRegisterId());
                break;
                
            case TRACK_CMD_REG_REQ:
                buf.put(m_transId);
                break;
        }
    }

    public void decodePosExtInd(ByteBuffer buf) {
        m_timeStamp = buf.getInt();

        m_latitude = buf.getInt();
        m_longitude = buf.getInt();
        m_speed = buf.getShort();
        m_course = buf.getShort();
        m_altitude = buf.getShort();
        m_userDatLength = buf.getShort();
    }

    public void decodePosComInd(ByteBuffer buf) {
        m_timeStamp = 0;
        m_speed = 0;
        m_course = 0;
        m_altitude = 0;
        m_userDatLength = 0;

        m_latitude = buf.getInt();
        m_longitude = buf.getInt();
    }

    public void decodeFromBuffer(ByteBuffer buf) {
        buf.order(ByteOrder.BIG_ENDIAN);
        head = new MessageHeader(buf);

        switch (head.getMessageId()) {
            case PDU.TRACK_REGISTER_REQ:
                version = (byte) buf.get();
                if (version == 1) {
                    LOGGER.info("Decoding TRACK_REGISTER_REQ");
                    byte[] imsi = new byte[8];
                    buf.get(imsi, 0, imsi.length);
                    int i = 0;
                    for (int j = 0; j < 15; j += 2, i++) {
                        m_imsi[j] = (byte) ((imsi[i] & 0xf0) >> 4);
                        m_imsi[j + 1] = (byte) (imsi[i] & 0x0f);
                    }

                    byte[] terminalId = new byte[8];
                    buf.get(terminalId, 0, terminalId.length);
                    i = 0;
                    for (int j = 0; j < 15; j += 2, i++) {
                        m_terminalId[j] = (byte) ((terminalId[i] & 0xf0) >> 4);
                        m_terminalId[j + 1] = (byte) ((terminalId[i] & 0x0f));
                    }

                    m_cmdValid = buf.get();
                } else {
                    LOGGER.info("Version != 1 (" + version + ")");
                }
                break;

            case PDU.TRACK_POS_EXT_IND:
                LOGGER.info("[" + head.getRegisterId() + "] Decoding TRACK_POS_EXT_IND");
                decodePosExtInd(buf);
                break;

            case PDU.TRACK_POS_COM_IND:
                LOGGER.info("[" + head.getRegisterId() + "] Decoding TRACK_POS_COM_IND");
                decodePosComInd(buf);
                break;
        }
    }

}
